$(function () {
  console.clear()
  console.log("%c👀发现你了，观你骨骼惊奇，天赋世所罕见，前途不可限量！😎。", "color:#382a88;font-weight:bold;font-size:2em")
  window.console = {}
})

$(function () {
  var title = document.title
  var titleTimer = null
  document.addEventListener('visibilitychange', function () {
    if (document.hidden) {
      document.title = '😠 死鬼，快回来~ ' + title;
      clearTimeout(titleTimer);
    }
    else {
      document.title = '😜 嘻嘻，想你啦~ ' + title
      titleTimer = setTimeout(function () {
        document.title = title
      }, 1e3)
    }
  })
})

$(function () {
  var header = $('#header'),
    navMenu = $('#nav-menu'),
    layout = $('#layout'),
    container = $('.body_container'),
    contentContainer = $('.content_container'),
    sidebar = $('#sidebar'),
    contentContainerHeight = contentContainer.eq(0).outerHeight(true),
    sidebarHeight = sidebar.outerHeight(true),
    navOffsetTop = navMenu.offset ? navMenu.offset().top : 0,
    headerHeight = header.outerHeight(true),
    headerWidth = header.outerWidth(),
    navHeight = navMenu.outerHeight(true),
    marginTop = container.css('margin-top'),
    marginBottom = container.css('margin-bottom'),
    marginLeft = container.css('margin-left'),
    marginRight = container.css('margin-right'),
    paddingLeft = container.css('padding-left'),
    paddingRight = container.css('padding-right'),
    headerStyle = { 'position': 'fixed', 'top': 0, 'left': paddingLeft, 'right': paddingRight, 'z-index': 9999, 'height': navHeight + 'px', 'width': headerWidth, 'margin': marginTop + ' ' + marginRight + ' ' + marginBottom + ' ' + marginLeft, 'padding': 0, 'background-color': '#fff' },
    timer = null

  $('.pure-u-md-1-4').after($('#layout > .google-auto-placed'))
  if (contentContainerHeight < sidebarHeight) contentContainer.css('min-height', sidebarHeight + 'px')

  fixedNav()

  $(window).scroll(function () {
    clearTimeout(timer)
    timer = setTimeout(function () {
      fixedNav()
    });
  })

  function fixedNav () {
    if (navOffsetTop - $(window).scrollTop() <= 0) {
      navMenu.siblings().hide()
      header.css(headerStyle)
      if (layout.prev().hasClass('google-auto-placed')) {
        header.next().css('padding-top', headerHeight)
        layout.css('padding-top', 0)
      }
      else layout.css('padding-top', headerHeight)
    } else {
      headerHeight = header.outerHeight(true)
      navOffsetTop = navMenu.offset ? navMenu.offset().top : 0
      navMenu.siblings().show()
      header.removeAttr('style')
      if (layout.prev().hasClass('google-auto-placed')) header.next().css('padding-top', 0)
      else layout.css('padding-top', 0)
    }
  }

})

$(function () {
  function grayFilter () {
    $('html').addClass('grayscale1')
  }
  /**
   * 12/13 南京大屠杀死难者国家公祭日
  */
  var _nowDate, _dates = ['12/13']
  _nowDate = new Date()
  var _m = _nowDate.getMonth() + 1
  var _d = _nowDate.getDate()
  var _md = `${_m}/${_d}`
  var _isGray = _dates.includes(_md)
  if (_isGray) {
    grayFilter()
  }
})

$(function () {
  if (window.location.hash) {
    var checkExist = setTimeout(function () {
      if ($(window.location.hash).length) {
        $('html, body').animate({ scrollTop: $(window.location.hash).offset().top - 50 }, 1000);
        clearInterval(checkExist);
      }
    }, 100);
  }
})